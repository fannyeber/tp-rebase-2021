# TP - GitLab
## Demarrage
```
git clone git@gitlab.com:fannyeber/tp-rebase-2021.git
cd tp-rebase-2021
git remote add personal git@gitlab.com:fannyeber/tp-rebase-2021.git
```
## Votre branche 
```
git branch 1-customize-readme
git switch 1-customize-readme
git add README.md
git commit -m "Modifications du readme"
git push origin 
```
On constate qu'on ne peut pas push notre branche courante sur origin
```
git push personal
```
Après que Julien ait merge mon projet mon issue aurait du se mettre en close (grace au numero de la branche qui est le même que celui de l'issue)

Mais à cause d'une mauvaise manipulation de ma part ça ne l'a pas fait (je l'ai donc close par moi-même).
La branche à été merge dans main
```
git switch main 
git pull
```
## Rapport
```
git branch 2-add-rapport
git switch 2-add-rapport
git add Rapport.md
git commit -m "Ajout du rapport"
git push personal
```